package com.chess.game.base.screen.transition;


import com.chess.game.beginplay.screen.BeginScreen;
import com.chess.game.beginplay.screen.LoginScreen;
import com.chess.game.beginplay.screen.MainGameScreen;
import com.chess.game.beginplay.screen.MenuScreen;

/**
 * TODO: Class description here.
 *
 * @author QuangBD
 * @name ScreenEnum
 * @licence Copyright (C) 2015 Uri Studio. All Rights Reserved.
 */
public enum ScreenEnum {
    MM_01 {
        @Override
        protected BaseScreen getScreen(Object... params) {
            return new BeginScreen();
        }
    },

    MM_02 {
        @Override
        protected BaseScreen getScreen(Object... params) {
            return new LoginScreen();
        }
    },

    MM_03 {
        @Override
        protected BaseScreen getScreen(Object... params) {
            return new MenuScreen();
        }
    },

    MM_04 {
        @Override
        protected BaseScreen getScreen(Object... params) {
            return new MainGameScreen();
        }
    };

    protected abstract BaseScreen getScreen(Object... params);
}
