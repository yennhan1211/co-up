package com.chess.game.base.screen.transition;

/**
 * Created by ThuyNinh on 10/17/2015.
 */
import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class TransitionManager extends BaseScreen {
    public BaseScreen inScreen;
    public BaseScreen outScreen;

    private Game game;

    public TransitionManager() {
        super();
        this.game = ScreenManager.getInstance().getGame();
    }

    public void fadeScreens(TransitionType type, BaseScreen nextScreen) {
        fadeScreens(type, nextScreen, 1f);
    }

    public void fadeScreens(TransitionType type, BaseScreen nextScreen, float duration) {
        BaseScreen inScreen = (BaseScreen) game.getScreen();
        if (inScreen instanceof TransitionManager) {
            inScreen = this.outScreen;
        }
        fadeScreens(type, inScreen, nextScreen, duration);
    }

    public void fadeScreens(TransitionType type, final BaseScreen currentScreen, final BaseScreen nextScreen, float duration) {
        this.inScreen = currentScreen;
        this.outScreen = nextScreen;

        nextScreen.stage.getRoot().clearActions();
        nextScreen.stage.getRoot().setPosition(0, 0);
        nextScreen.stage.getRoot().setColor(Color.WHITE);
        currentScreen.stage.getRoot().clearActions();
        currentScreen.stage.getRoot().setPosition(0, 0);
        currentScreen.stage.getRoot().setColor(Color.WHITE);

        currentScreen.hide();
        nextScreen.show();

        game.setScreen(this);
//        Gdx.input.setInputProcessor(new InputMultiplexer(nextScreen, nextScreen.stage));

        switch (type) {
            case SLIDE_LEFT_RIGHT:
                nextScreen.stage.getRoot().setPosition(-nextScreen.stage.getWidth(), 0);
                nextScreen.stage.addAction(Actions.sequence(Actions.moveTo(0, 0, duration, Interpolation.exp10Out)));
                currentScreen.stage.addAction(Actions.sequence(Actions.moveTo(currentScreen.stage.getWidth(), 0, duration, Interpolation.exp10Out),
                        Actions.run(new Runnable() {
                            @Override
                            public void run() {
                                game.setScreen(nextScreen);
                            }
                        })));
                break;
            case SLIDE_RIGHT_LEFT:
                nextScreen.stage.getRoot().setPosition(nextScreen.stage.getWidth(), 0);
                nextScreen.stage.addAction(Actions.sequence(Actions.moveTo(0, 0, duration, Interpolation.exp10Out)));
                currentScreen.stage.addAction(Actions.sequence(Actions.moveTo(-currentScreen.stage.getWidth(), 0, duration, Interpolation.exp10Out),
                        Actions.run(new Runnable() {
                            @Override
                            public void run() {
                                game.setScreen(nextScreen);
                            }
                        })));
                break;
            case SLIDE_UP_DOWN:
                nextScreen.stage.getRoot().setPosition(0, -nextScreen.stage.getHeight());
                nextScreen.stage.addAction(Actions.sequence(Actions.moveTo(0, 0, duration, Interpolation.exp10Out)));
                currentScreen.stage.addAction(Actions.sequence(Actions.moveTo(0, currentScreen.stage.getHeight(), duration, Interpolation.exp10Out),
                        Actions.run(new Runnable() {
                            @Override
                            public void run() {
                                game.setScreen(nextScreen);
                            }
                        })));
                break;
            case SLIDE_DOWN_UP:
                nextScreen.stage.getRoot().setPosition(0, nextScreen.stage.getHeight());
                nextScreen.stage.addAction(Actions.sequence(Actions.moveTo(0, 0, duration, Interpolation.exp10Out)));
                currentScreen.stage.addAction(Actions.sequence(Actions.moveTo(0, -currentScreen.stage.getHeight(), duration, Interpolation.exp10Out),
                        Actions.run(new Runnable() {
                            @Override
                            public void run() {
                                game.setScreen(nextScreen);
                            }
                        })));
                break;
            case FADE:
                nextScreen.stage.addAction(Actions.sequence(Actions.fadeIn(duration)));
                currentScreen.stage.addAction(Actions.sequence(Actions.fadeOut(duration), Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        game.setScreen(nextScreen);
                    }
                }), Actions.color(Color.WHITE)));
                break;
        }
    }

    @Override
    public void buildStage() {

    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        outScreen.render(delta);
        inScreen.render(delta);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
