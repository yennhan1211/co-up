package com.chess.game.base.screen.view;

/**
 * An interface for all classes that needs to do rendering to a screen.
 *
 * @author QuangBD
 * @name Renderer
 * @licence Copyright (C) 2015 Uri Studio. All Rights Reserved.
 */
public interface Renderer {
    /**
     * Should be called when rendering needs to be done.
     *
     * @param delta
     *            The time in seconds since the last render.
     */
    void render(float delta);

    /**
     * Should be called when device screen is resized.
     *
     * @param width
     *            The new width in pixels.
     * @param height
     *            The new height in pixels.
     */
    void setSize(int width, int height);

    /**
     * Should be called when the renderer needs to release all resources.
     */
    void dispose();
}
