package com.chess.game.base.screen.transition;

import com.badlogic.gdx.*;

/**
 * TODO: Class description here.
 *
 * @author QuangBD
 * @name ScreenManager
 * @licence Copyright (C) 2015 Uri Studio. All Rights Reserved.
 */
public final class ScreenManager {
    /** Singleton: unique instance. */
    private static ScreenManager instance;
    /** Reference to game. */
    private Game game;
    /**
     * Singleton: private constructor.
     */
    private ScreenManager() {
        super();
    }

    /**
     * Singleton: retrieve instance.
     * @return
     */
    public static ScreenManager getInstance() {
        if(instance == null) {
            instance = new ScreenManager();
        }
        return instance;
    }

    /**
     * Initialization with the game class.
     * @param game
     */
    public void initialize(Game game) {
        this.game = game;
    }

    // Show in the game the screen which enum type is received
    public void showScreen(ScreenEnum screenEnum, Object... params) {

        // Get current screen to dispose it
        Screen currentScreen = game.getScreen();

        // Show new screen
        BaseScreen newScreen = screenEnum.getScreen(params);
        newScreen.buildStage();
        game.setScreen(newScreen);

        // Dispose previous screen
        if (currentScreen != null) {
            currentScreen.dispose();
        }
    }

    public Game getGame() {
        return game;
    }
}
