package com.chess.game.base.screen.transition;

/**
 * Created by ThuyNinh on 10/16/2015.
 */
public enum TransitionType {
    FADE, SLIDE_LEFT_RIGHT, SLIDE_RIGHT_LEFT, SLIDE_UP_DOWN, SLIDE_DOWN_UP
}
