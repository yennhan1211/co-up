package com.chess.game.base.screen.storage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetErrorListener;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.MusicLoader;
import com.badlogic.gdx.assets.loaders.TextureAtlasLoader;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.*;

/**
 * TODO: Class description here.
 *
 * @author QuangBD
 * @name Assets
 * @licence Copyright (C) 2015 Uri Studio. All Rights Reserved.
 */
public class Assets implements Disposable, AssetErrorListener{
    private final String TAG = Assets.class.getName();
    private Logger logger;
    private AssetManager manager;
    private ObjectMap<String, Array<Asset>> groups;

    public Assets(String assetFile) {
        logger = new Logger(TAG, Logger.INFO);
        manager = new AssetManager();
        manager.setErrorListener(this);
//        manager.setLoader(PhysicsData.class, new PhysicsLoader(new InternalFileHandleResolver()));
//        manager.setLoader(AnimationData.class, new AnimationLoader(new InternalFileHandleResolver()));
        manager.setLoader(TextureAtlas.class, new TextureAtlasLoader(new InternalFileHandleResolver()));
        manager.setLoader(Music.class, new MusicLoader(new InternalFileHandleResolver()));

        loadGroups(assetFile);
    }

    public void loadGroup(String groupName) {
        logger.info("loading group " + groupName);

        Array<Asset> assets = groups.get(groupName, null);

        if(assets != null) {
            for (Asset asset : assets) {
                manager.load(asset.path, asset.type);
            }
        }
        else {
            logger.error("error loading group " + groupName + ", not found");
        }
    }

    public void unloadGroup(String groupName) {
        logger.info("unloading group " + groupName);

        Array<Asset> assets = groups.get(groupName, null);

        if (assets != null) {
            for (Asset asset : assets) {
                if (manager.isLoaded(asset.path, asset.type)) {
                    manager.unload(groupName);
                }
            }
        }
        else {
            logger.error("error unloading group " + groupName + ", not found");
        }
    }

    public synchronized <T> T get(String fileName) {
        return manager.get(fileName);
    }

    public synchronized <T> T get(String fileName, Class<T> type) {
        return manager.get(fileName, type);
    }

    public boolean update() {
        return manager.update();
    }

    public void finishLoading() {
        manager.finishLoading();
    }

    public float getProgress() {
        return manager.getProgress();
    }

    @Override
    public void error(AssetDescriptor asset, Throwable throwable) {
        logger.error("error loading " + asset.fileName);
    }

    @Override
    public void dispose() {
        logger.info("shutting down");
        manager.dispose();
    }

    private void loadGroups(String assetFile) {
        groups = new ObjectMap<String, Array<Asset>>();

        logger.info("loading file " + assetFile);

        try {
            XmlReader reader = new XmlReader();
            XmlReader.Element root = reader.parse(Gdx.files.internal(assetFile));

            for (XmlReader.Element groupElement : root.getChildrenByName("group")) {
                String groupName = groupElement.getAttribute("name", "base");

                if (groups.containsKey(groupName)) {
                    logger.error("group " + groupName + " already exists, skipping");
                    continue;
                }

                logger.info("registering group " + groupName);

                Array<Asset> assets = new Array<Asset>();

                for (XmlReader.Element assetElement : groupElement.getChildrenByName("asset")) {
                    assets.add(new Asset(assetElement.getAttribute("type", ""), assetElement.getAttribute("path", "")));
                }

                groups.put(groupName, assets);
            }
        }
        catch (Exception e) {
            logger.error("error loading file " + assetFile + " " + e.getMessage());
        }
    }

    private class Asset {
        public Class<?> type;
        public String path;

        public Asset(String type, String path) {
            try {
                this.type = Class.forName(type);
                this.path = path;
            }
            catch (ClassNotFoundException e) {
                logger.error("asset type " + type + " not found");
            }
        }
    }

    public static Skin skin;
    public static Music menuMusic;
    public static TextureAtlas gameAtlas;

    /**
     * Loads all assets required by {@link BeginScreen}.
     */
    public static void loadBegin(){
        skin = new Skin(Gdx.files.internal("skin-labels.json"));
        menuMusic = Gdx.audio
                .newMusic(Gdx.files.internal("music/fortress.ogg"));
    }

    /**
     * Disposes of all assets required by {@link BeginScreen}.
     */
    public static void disposeBegin() {
        skin.dispose();
        menuMusic.dispose();
    }

    /**
     * Loads all assets required by {@link GameScreen}.
     */
    public static void loadGame() {
        gameAtlas = new TextureAtlas(
                Gdx.files.internal("atlases/open-chess-atlas.atlas"));
        skin = new Skin(Gdx.files.internal("skin-all.json"), gameAtlas);
    }

    /**
     * Disposes of all assets required by {@link GameScreen}.
     */
    public static void disposeGame() {
        gameAtlas.dispose();
    }
}
