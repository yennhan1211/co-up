package com.chess.game.base.screen;

import com.badlogic.gdx.Game;
import com.chess.game.base.screen.transition.ScreenEnum;
import com.chess.game.base.screen.transition.ScreenManager;

/**
 * MainGame game class. Used just for transitioning between different screens.
 */
public class MainGame extends Game {

    @Override
    public void create() {
        ScreenManager.getInstance().initialize(this);
        ScreenManager.getInstance().showScreen(ScreenEnum.MM_01);
    }

}
