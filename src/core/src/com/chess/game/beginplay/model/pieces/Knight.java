package com.chess.game.beginplay.model.pieces;

import com.chess.game.beginplay.model.Move;
import com.chess.game.beginplay.model.Piece;

/**
 * Represents a knight chess piece.
 *
 * @author Quang
 * @name Knight
 * @licence Copyright (C) 2015 Uri Studio. All Rights Reserved.
 */
public class Knight extends Piece {
    /**
     * Creates a new chess piece on a tile.
     *
     * @param x          Horizontal index of the tile.
     * @param y          Vertical index of the tile.
     * @param isWhite    Determines whether the chess piece is white or black.
     */
    public Knight(int x, int y, boolean isWhite) {
        super(x, y, isWhite, isWhite ? "white-knight" : "black-knight");

        /* Add valid moves. */
        this.validMoves.add(new Move(1, 2, false));
        this.validMoves.add(new Move(2, 1, false));
        this.validMoves.add(new Move(1, -2, false));
        this.validMoves.add(new Move(2, -1, false));
        this.validMoves.add(new Move(-1, -2, false));
        this.validMoves.add(new Move(-2, -1, false));
        this.validMoves.add(new Move(-1, 2, false));
        this.validMoves.add(new Move(-2, 1, false));
    }
}
