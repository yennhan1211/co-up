package com.chess.game.beginplay.model;

/**
 * Defines a type of move that a chess piece can make.
 *
 * @author Quang
 * @name Move
 * @licence Copyright (C) 2015 Uri Studio. All Rights Reserved.
 */
public class Move {
    public int xOffset;
    public int yOffset;
    public boolean continuous;

    /**
     * Creates a move. When creating a new <code>Move</code> instance, keep in
     * mind that they are created according to white chess pieces. Y coordinate
     * inversion for black pieces is done automatically.
     *
     * @param xOffset    Horizontal tile offset.
     * @param yOffset    Vertical tile offset.
     * @param continuous If true, than xOffset and yOffset are interpreted as unit
     *                   vector components which show a direction that can be moved on
     *                   continuously.
     */
    public Move(int xOffset, int yOffset, boolean continuous) {
        this.xOffset = xOffset;
        this.yOffset = yOffset;
        this.continuous = continuous;
    }
}
