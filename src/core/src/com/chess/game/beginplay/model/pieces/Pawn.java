package com.chess.game.beginplay.model.pieces;

import com.chess.game.beginplay.model.Board;
import com.chess.game.beginplay.model.Move;
import com.chess.game.beginplay.model.Piece;

/**
 * Represents a pawn chess piece.
 *
 * @author Quang
 * @name Pawn
 * @licence Copyright (C) 2015 Uri Studio. All Rights Reserved.
 */
public class Pawn extends Piece {
    /**
     * Creates a new chess piece on a tile.
     *
     * @param x       Horizontal index of the tile.
     * @param y       Vertical index of the tile.
     * @param isWhite Determines whether the chess piece is white or black.
     */
    public Pawn(int x, int y, boolean isWhite) {
        super(x, y, isWhite, isWhite ? "white-pawn" : "black-pawn");
        this.canCaptureWithMove = false;

        /* Add valid moves. */
        this.validMoves.add(new Move(0, 1, false));
        this.validMoves.add(new Move(0, 2, false));

        /* Add capture-only moves. */
        this.captureOnlyMoves.add(new Move(1, 1, false));
        this.captureOnlyMoves.add(new Move(-1, 1, false));
    }

    @Override
    public void moved() {
        Board board = (Board) this.getParent();
        int x = (int) this.getX();
        int y = (int) this.getY();
        /* Ensure pawns can move 2 tiles forward the first time only. */
        if (this.validMoves.size == 2) {
            this.validMoves.pop();
        }
        /* Pawn promotion. */
        if ((this.isWhite && (y == 7)) || (!this.isWhite && (y == 0))) {
            board.removePieceAt(x, y);
            board.addPiece(new Queen(x, y, this.isWhite));
        }
    }
}
