package com.chess.game.beginplay.model.pieces;

import com.chess.game.beginplay.model.Move;
import com.chess.game.beginplay.model.Piece;

/**
 * Represents a queen chess piece.
 *
 * @author Quang
 * @name Queen
 * @licence Copyright (C) 2015 Uri Studio. All Rights Reserved.
 */
public class Queen extends Piece {
    /**
     * Creates a new chess piece on a tile.
     *
     * @param x          Horizontal index of the tile.
     * @param y          Vertical index of the tile.
     * @param isWhite    Determines whether the chess piece is white or black.
     */
    public Queen(int x, int y, boolean isWhite) {
        super(x, y, isWhite, isWhite ? "white-queen" : "black-queen");

        /* Add valid moves. */
        this.validMoves.add(new Move(0, 1, true));
        this.validMoves.add(new Move(1, 1, true));
        this.validMoves.add(new Move(1, 0, true));
        this.validMoves.add(new Move(1, -1, true));
        this.validMoves.add(new Move(0, -1, true));
        this.validMoves.add(new Move(-1, -1, true));
        this.validMoves.add(new Move(-1, 0, true));
        this.validMoves.add(new Move(-1, 1, true));
    }
}
