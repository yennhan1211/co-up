package com.chess.game.beginplay.model.pieces;

import com.badlogic.gdx.utils.Array;
import com.chess.game.beginplay.model.Board;
import com.chess.game.beginplay.model.Move;
import com.chess.game.beginplay.model.Piece;
import com.chess.game.beginplay.model.Tile;

/**
 * Represents a king chess piece.
 *
 * @author Quang
 * @name King
 * @licence Copyright (C) 2015 Uri Studio. All Rights Reserved.
 */
public class King extends Piece {

    private boolean moved;

    public King(int x, int y, boolean isWhite) {
        super(x, y, isWhite, isWhite ? "white-king" : "black-king");

        /* Add valid moves. */
        this.validMoves.add(new Move(0, 1, false));
        this.validMoves.add(new Move(1, 1, false));
        this.validMoves.add(new Move(1, 0, false));
        this.validMoves.add(new Move(1, -1, false));
        this.validMoves.add(new Move(0, -1, false));
        this.validMoves.add(new Move(-1, -1, false));
        this.validMoves.add(new Move(-1, 0, false));
        this.validMoves.add(new Move(-1, 1, false));
    }

    @Override
    public Array<Tile> getValidMoveTiles(Board board, boolean checkFriendly) {
        Array<Tile> tiles = super.getValidMoveTiles(board, checkFriendly);
        int x = (int) this.getX();
        int y = (int) this.getY();

        /* Show castling moves if available. */
        if (!this.moved) {

            if ((board.getPieceAt(x + 1, y) == null) && (board.getPieceAt(x + 2, y) == null)) {
                this.validMoves.add(new Move(2, 0, false));
            }

            if ((board.getPieceAt(x - 1, y) == null && (board.getPieceAt(x - 2, y) == null)
                    && (board.getPieceAt(x - 3, y) == null))) {
                this.validMoves.add(new Move(-2, 0, false));
            }
        }
        return tiles;
    }

    @Override
    public void moved() {
        int x = (int) this.getX();

        /* Handle castling move. */
        if (!this.moved) {
            this.moved = true;

            if ((x == 6) || (x == 2)) {
                Board board = (Board) this.getParent();
                int y = (int) this.getY();
                Piece rook = (x == 2) ? board.getPieceAt(0, y) : board.getPieceAt(7, y);

                if ((rook == null) || !(rook instanceof Rook)) {
                    return;
                }

                if ((x == 2) && (board.getPieceAt(1, y) == null) && (board.getPieceAt(3, y) == null)) {
                    board.relocatePieceAt(0, y, 3, y);
                } else if ((x == 6) && (board.getPieceAt(5, y) == null)) {
                    board.relocatePieceAt(7, y, 5, y);
                }
            }
        }
    }
}
