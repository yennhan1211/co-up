package com.chess.game.beginplay.screen;

import com.badlogic.gdx.Gdx;
import com.chess.game.base.screen.storage.Assets;
import com.chess.game.base.screen.transition.BaseScreen;
import com.chess.game.base.screen.transition.ScreenEnum;
import com.chess.game.base.screen.transition.ScreenManager;
import com.chess.game.beginplay.view.BeginRenderer;

/**
 * TODO: Class description here.
 *
 * @author Quang
 * @name BeginScreen
 * @licence Copyright (C) 2015 Uri Studio. All Rights Reserved.
 */
public class BeginScreen extends BaseScreen {
    private BeginRenderer mBeginRenderer;


    @Override
    public void buildStage() {

    }

    @Override
    public void show() {
        Assets.loadBegin();

        Assets.menuMusic.setLooping(true);
        Assets.menuMusic.play();

        this.mBeginRenderer = new BeginRenderer();
        this.mBeginRenderer
                .setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }

    @Override
    public void render(float delta) {
        this.mBeginRenderer.render(delta);
        if (Gdx.input.justTouched()) {
            ScreenManager.getInstance().showScreen(ScreenEnum.MM_02);
        }
    }

    @Override
    public void resize(int width, int height) {
        this.mBeginRenderer.setSize(width, height);
    }

    @Override
    public void pause() {
        Assets.menuMusic.pause();
    }

    @Override
    public void resume() {
        Assets.menuMusic.play();
    }

    @Override
    public void hide() {
        this.mBeginRenderer.dispose();
        Assets.disposeBegin();
    }

    @Override
    public void dispose() {
        // Never called automatically.
    }
}
