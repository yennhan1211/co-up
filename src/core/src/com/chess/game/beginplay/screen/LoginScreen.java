package com.chess.game.beginplay.screen;

import com.badlogic.gdx.Gdx;
import com.chess.game.base.screen.transition.BaseScreen;
import com.chess.game.beginplay.view.LoginRenderer;

/**
 * TODO: Class description here.
 *
 * @author Quang
 * @name LoginScreen
 * @licence Copyright (C) 2015 Uri Studio. All Rights Reserved.
 */
public class LoginScreen extends BaseScreen {

    private LoginRenderer mLoginRenderer;

    @Override
    public void buildStage() {

    }

    @Override
    public void show() {
        mLoginRenderer = new LoginRenderer(stage);
        this.mLoginRenderer
                .setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }

    @Override
    public void render(float delta) {
        mLoginRenderer.render(delta);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        this.mLoginRenderer.dispose();
    }

    @Override
    public void dispose() {

    }
}
