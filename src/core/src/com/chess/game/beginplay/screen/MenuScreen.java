package com.chess.game.beginplay.screen;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.chess.game.base.screen.transition.BaseScreen;
import com.chess.game.base.screen.transition.ScreenManager;
import com.chess.game.beginplay.view.MenuRenderer;

/**
 * TODO: Class description here.
 *
 * @author Quang
 * @name MenuScreen
 * @licence Copyright (C) 2015 Uri Studio. All Rights Reserved.
 */
public class MenuScreen extends BaseScreen {

    private MenuRenderer mMenuRenderer;

    private Game mGame;

    @Override
    public void buildStage() {

    }

    @Override
    public void show() {
        mGame = ScreenManager.getInstance().getGame();
        this.mMenuRenderer = new MenuRenderer(mGame);
        this.mMenuRenderer
                .setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }

    @Override
    public void render(float delta) {
        this.mMenuRenderer.render(delta);
    }

    @Override
    public void resize(int width, int height) {
        this.mMenuRenderer.setSize(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        this.mMenuRenderer.dispose();
    }

    @Override
    public void dispose() {

    }
}
