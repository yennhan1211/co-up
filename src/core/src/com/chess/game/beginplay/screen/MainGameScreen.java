package com.chess.game.beginplay.screen;

import com.badlogic.gdx.Gdx;
import com.chess.game.base.screen.storage.Assets;
import com.chess.game.base.screen.transition.BaseScreen;
import com.chess.game.beginplay.model.Board;
import com.chess.game.beginplay.view.MainGameRenderer;

/**
 * Created by ThuyNinh on 10/16/2015.
 */
public class MainGameScreen extends BaseScreen {
    private MainGameRenderer renderer;

    @Override
    public void buildStage() {

    }

    @Override
    public void show() {
        Board board; // Can't call the constructor here. Assets have to be loaded first.

        Assets.loadGame();
        board = new Board();
        board.populate();
        this.renderer = new MainGameRenderer(board);
        this.renderer
                .setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }

    @Override
    public void render(float delta) {
        this.renderer.render(delta);
    }

    @Override
    public void resize(int width, int height) {
        this.renderer.setSize(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        this.renderer.dispose();
        Assets.disposeGame();
    }

    @Override
    public void dispose() {

    }
}
