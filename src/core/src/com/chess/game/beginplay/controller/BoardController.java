package com.chess.game.beginplay.controller;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.utils.Array;
import com.chess.game.base.screen.controller.BaseController;
import com.chess.game.beginplay.model.Board;
import com.chess.game.beginplay.model.Piece;
import com.chess.game.beginplay.model.Tile;

/**
 * This class is under heavy development. As it keeps changing with rapid speed,
 * it'll only receive proper javadoc comments when it's close to completion.
 *
 * @author Quang
 * @name BoardController
 * @licence Copyright (C) 2015 Uri Studio. All Rights Reserved.
 */
public class BoardController extends BaseController {
    private final Board board;
    private final Array<Tile> highlightedTiles = new Array<Tile>();

    public BoardController(Board board) {
        this.board = board;
    }

    @Override
    public void tap(InputEvent event, float x, float y, int count, int button) {
        Actor target = event.getTarget(); // Tapped actor.
        int tx = (int) target.getX(); // Tapped tile x.
        int ty = (int) target.getY(); // Tapped tile y.

        if (target.getClass().getSuperclass().equals(Piece.class)) {
            Piece piece = (Piece) target;

            if ((((this.board.round % 2) == 0) && piece.isWhite)
                    || (((this.board.round % 2) == 1) && !piece.isWhite)) {
                this.selectPiece(piece);
            } else {
                this.movePiece(this.board.selectedPiece, tx, ty);
            }
        } else {
            this.movePiece(this.board.selectedPiece, tx, ty);
        }
    }

    private void movePiece(Piece piece, int x, int y) {

		/* Check move validity. */
        if ((piece == null) || !this.board.getTileAt(x, y).isHighlighted) {
            return;
        }

        int xOld = (int) piece.getX();
        int yOld = (int) piece.getY();

		/* Remove highlights. */
        this.removeMoveHighlights();

		/* Capture. */
        if (this.board.getPieceAt(x, y) != null) {
            this.board.removePieceAt(x, y);
        }

		/* Move. */
        this.board.relocatePieceAt(xOld, yOld, x, y);
        this.board.selectedPiece.moved();

		/* Deselect and advance round. */
        this.board.selectedPiece = null;
        this.board.round++;
    }

    private void selectPiece(Piece piece) {
        this.removeMoveHighlights();
        this.board.selectedPiece = piece;
        this.addMoveHighlightsForPiece(piece);
    }

    // TODO: Complete before writing javadoc comments for this.
    private void addMoveHighlightsForPiece(Piece piece) {
        Array<Tile> tiles = piece.getValidMoveTiles(this.board, true);

        tiles.addAll(piece.getCaptureOnlyTiles(this.board, true));

        for (Tile tile : tiles) {
            int tx = (int) tile.getX();
            int ty = (int) tile.getY();

			/* Make sure the move doesn't put the king in check. */
            if (this.board.isMoveSafe(piece, tx, ty)) {
                this.highlightedTiles.add(tile);
                tile.isHighlighted = true;
            }
        }
    }

    private void removeMoveHighlights() {

        while (this.highlightedTiles.size > 0) {
            this.highlightedTiles.pop().isHighlighted = false;
        }
    }
}
