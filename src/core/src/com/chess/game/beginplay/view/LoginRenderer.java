package com.chess.game.beginplay.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.chess.game.base.screen.transition.TransitionManager;
import com.chess.game.base.screen.transition.TransitionType;
import com.chess.game.base.screen.view.Renderer;
import com.chess.game.beginplay.screen.MenuScreen;

/**
 * Created by ThuyNinh on 10/16/2015.
 */
public class LoginRenderer implements Renderer {

    private Stage stage; //** stage holds the Button **//
    private BitmapFont font; //** same as that used in Tut 7 **//
    private TextureAtlas buttonsAtlas; //** image of buttons **//
    private Skin buttonSkin; //** images are used as skins of the button **//
    private TextButton button; //** the button - the only actor in program **//
    private TextButton.TextButtonStyle mTextButtonStyle;

    TransitionManager mTransitionManager;

    public LoginRenderer(Stage mStage) {
        this.stage = mStage;
        Gdx.input.setInputProcessor(stage);
        mTransitionManager = new TransitionManager();

        buttonsAtlas = new TextureAtlas(Gdx.files.internal("atlases/button.atlas"));
        buttonSkin = new Skin();
        buttonSkin.addRegions(buttonsAtlas);
        mTextButtonStyle = new TextButton.TextButtonStyle();
        mTextButtonStyle.up = buttonSkin.getDrawable("btn_normal");
        mTextButtonStyle.down = buttonSkin.getDrawable("btn_press");
        font = new BitmapFont(Gdx.files.internal("atlases/customfont.fnt"), false); //** font
        mTextButtonStyle.font = font;

        button = new TextButton("Login", mTextButtonStyle);
//        button.setPosition(stage.getWidth() / 2, stage.getHeight() / 2);
        button.setHeight(50);
        button.setWidth(200);

        button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("addListener","clicked");
//                switchScreen(game, new BeginScreen());

                mTransitionManager.fadeScreens(TransitionType.FADE, new MenuScreen());

            }
        });

        stage.addActor(button);
    }

//    public void switchScreen(final Game mGame, final ScreenEnum newScreen){
//        SequenceAction sequenceAction = new SequenceAction();
//        sequenceAction.addAction(fadeOut(0.5f));
//        sequenceAction.addAction(run(new Runnable() {
//            @Override
//            public void run() {
//                ((Game) Gdx.app.getApplicationListener()).setScreen(newScreen);
//                Gdx.app.log("addAction","clicked");
//            }
//        }));
//        stage.addAction(sequenceAction);
//    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(.3f, .3f, .4f, 1);
        Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void setSize(int width, int height) {
        this.stage.getViewport().update(width, height);
        this.positionUI();
        Gdx.graphics.requestRendering();
    }

    @Override
    public void dispose() {
//        stage.dispose();
        buttonsAtlas.dispose();
        buttonSkin.dispose();
    }

    /**
     * Positions UI elements.
     */
    public void positionUI() {
        float cX = this.stage.getWidth() / 2; // X coordinate of screen's
        // center.
        float cY = this.stage.getHeight() / 2; // Y coordinate of screen's
        // center.
        this.button.setPosition(cX - (this.button.getWidth() / 2), cY);
    }
}
