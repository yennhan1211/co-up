package com.chess.game.beginplay.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.chess.game.base.screen.storage.Assets;
import com.chess.game.base.screen.transition.ScreenEnum;
import com.chess.game.base.screen.transition.ScreenManager;
import com.chess.game.base.screen.view.Renderer;
import com.chess.game.beginplay.model.Board;

/**
 * Main game screen. Creates a new chess board and a game renderer, then tells
 * the renderer to render that board.
 *
 * @author Quang
 * @name MainGameRenderer
 * @licence Copyright (C) 2015 Uri Studio. All Rights Reserved.
 */
public class MainGameRenderer implements Renderer {
    private final Stage stage = new Stage(new FitViewport(8, 10));
    private Table hud;
    private TextButton resetButton;
    private TextButton menuButton;

    public MainGameRenderer(Board board) {
        Gdx.input.setInputProcessor(this.stage);
        this.stage.addActor(board);
        this.initUI();
    }

    private void initUI() {
        this.hud = new Table(Assets.skin);

        this.resetButton = new TextButton(" Reset Game ", Assets.skin);
        this.resetButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ScreenManager.getInstance().showScreen(ScreenEnum.MM_04);
            }
        });
        this.menuButton = new TextButton(" Start Game", Assets.skin);
        this.menuButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ScreenManager.getInstance().showScreen(ScreenEnum.MM_01);
            }
        });

        this.hud.add(this.resetButton);
        this.hud.add(this.menuButton);
        this.hud.setTransform(true);
        this.hud.setScale(1 / this.resetButton.getHeight());
        this.hud.setPosition(4, 9);
        this.stage.addActor(this.hud);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(.3f, .3f, .4f, 1);
        Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);
        this.stage.draw();
    }

    @Override
    public void setSize(int width, int height) {
        this.stage.getViewport().update(width, height, false);
        Gdx.graphics.requestRendering();
    }

    @Override
    public void dispose() {
        this.stage.dispose();
    }
}
