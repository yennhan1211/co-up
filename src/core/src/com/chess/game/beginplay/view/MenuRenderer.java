package com.chess.game.beginplay.view;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.chess.game.base.screen.transition.ScreenEnum;
import com.chess.game.base.screen.transition.ScreenManager;
import com.chess.game.base.screen.view.Renderer;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;

/**
 * TODO: Class description here.
 *
 * @author Quang
 * @name MenuRenderer
 * @licence Copyright (C) 2015 Uri Studio. All Rights Reserved.
 */
public class MenuRenderer extends ClickListener implements Renderer {
    private Stage mStage;
    private BitmapFont mBitmapFont;
    private TextureAtlas mButtonsAtlas;
    private Skin mButtonSkin;
    private TextButton mButton1;
    private TextButton mButton2;
    private TextButton mButton3;
    private TextButton.TextButtonStyle mTextButtonStyle;

    private Game mGame;

    public MenuRenderer(Game game) {
        this.mGame = game;
        mStage = new Stage();
        Gdx.input.setInputProcessor(mStage);

        mButtonsAtlas = new TextureAtlas(Gdx.files.internal("atlases/menu_btn.atlas"));
        mButtonSkin = new Skin();
        mButtonSkin.addRegions(mButtonsAtlas);
        mTextButtonStyle = new TextButton.TextButtonStyle();
        mTextButtonStyle.up = mButtonSkin.getDrawable("btn_yes_normal");
        mTextButtonStyle.down = mButtonSkin.getDrawable("btn_yes_press");
        mBitmapFont = new BitmapFont(Gdx.files.internal("atlases/menu_screen_font.fnt"), false);
        mTextButtonStyle.font = mBitmapFont;

        mButton1 = new TextButton("Tap su", mTextButtonStyle);
        mButton1.setPosition(mStage.getWidth() / 4, mStage.getHeight() / 4);
        mButton1.setHeight(50);
        mButton1.setWidth(200);
        mButton1.addListener(this);
        mStage.addActor(mButton1);

        mButton2 = new TextButton("Nghiep du", mTextButtonStyle);
        mButton2.setPosition(mStage.getWidth() / 4, mStage.getHeight() / 4);
        mButton2.setHeight(50);
        mButton2.setWidth(300);
        mButton2.addListener(this);
        mStage.addActor(mButton2);

        mButton3 = new TextButton("Chuyen nghiep", mTextButtonStyle);
        mButton3.setPosition(mStage.getWidth() / 4, mStage.getHeight() / 4);
        mButton3.setHeight(50);
        mButton3.setWidth(300);
        mButton3.addListener(this);
        mStage.addActor(mButton3);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(.3f, .3f, .4f, 1);
        Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);
        mStage.draw();
    }

    @Override
    public void setSize(int width, int height) {
        this.mStage.getViewport().update(width, height);
        this.positionUI();
        Gdx.graphics.requestRendering();
    }

    @Override
    public void dispose() {
        this.mStage.dispose();
    }

    @Override
    public void clicked(InputEvent event, float x, float y) {
        mStage.getRoot().getColor().a = 0;
        mStage.getRoot().addAction(fadeOut(1.5f));

        Actor actor = event.getListenerActor();
        if (actor == mButton1) {
            ScreenManager.getInstance().showScreen(ScreenEnum.MM_04);
        }
        else if (actor == mButton2) {
            ScreenManager.getInstance().showScreen(ScreenEnum.MM_04);
        }
        else if (actor == mButton3) {
            ScreenManager.getInstance().showScreen(ScreenEnum.MM_01);
        }
    }

    /**
     * Positions UI elements.
     */
    public void positionUI() {
        float cX = this.mStage.getWidth() / 2; // X coordinate of screen's
        // center.
        float cY = this.mStage.getHeight() / 2; // Y coordinate of screen's
        // center.

        this.mButton1.setPosition(cX - (this.mButton1.getWidth() / 2), cY);
        this.mButton2.setPosition(cX - (this.mButton2.getWidth() / 2), cY
                - this.mButton2.getHeight() - 2);
        this.mButton3.setPosition(cX - (this.mButton3.getWidth() / 2), cY
        - this.mButton2.getHeight() - this.mButton3.getHeight() - 4);
    }
}
