package com.chess.game.beginplay.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.chess.game.base.screen.storage.Assets;
import com.chess.game.base.screen.view.Renderer;

/**
 * TODO: Class description here.
 *
 * @author Quang
 * @name BeginRenderer
 * @licence Copyright (C) 2015 Uri Studio. All Rights Reserved.
 */
public class BeginRenderer implements Renderer {
    private final Stage mStage = new Stage(new FitViewport(480, 320));

    private Label mTitleLabel;
    private Label mStartLabel;

    public BeginRenderer() {
        this.initUI();
    }

    /**
     * Initializes UI elements.
     */
    private void initUI() {
        this.mTitleLabel = new Label("Welcome", Assets.skin, "title");
        this.mTitleLabel.setWrap(true);

        this.mStartLabel = new Label("Tap to Play", Assets.skin);
        this.mStartLabel.setWrap(true);

        this.mStage.addActor(this.mTitleLabel);
        this.mStage.addActor(this.mStartLabel);
    }

    /**
     * Positions UI elements.
     */
    public void positionUI() {
        float cX = this.mStage.getWidth() / 2; // X coordinate of screen's
        // center.
        float cY = this.mStage.getHeight() / 2; // Y coordinate of screen's
        // center.

        this.mTitleLabel.setPosition(cX - (this.mTitleLabel.getWidth() / 2), cY);
        this.mStartLabel.setPosition(cX - (this.mStartLabel.getWidth() / 2), cY
                - this.mStartLabel.getHeight());
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);
        this.mStage.draw();
    }

    @Override
    public void setSize(int width, int height) {
        this.mStage.getViewport().update(width, height);
        this.positionUI();
        Gdx.graphics.requestRendering();
    }

    @Override
    public void dispose() {
        this.mStage.dispose();
    }
}
