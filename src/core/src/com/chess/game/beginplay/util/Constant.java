package com.chess.game.beginplay.util;

/**
 * TODO: Class description here.
 *
 * @author Quang
 * @name Constant
 * @licence Copyright (C) 2015 Uri Studio. All Rights Reserved.
 */
public class Constant {
    /**
     * Unit width of the screen. Everything in this game is rendered in game
     * units instead of pixels for scalability. This variable determines the
     * width of the screen in terms of game units. It is important to note that
     * each chess piece is 1 unit wide and 1 unit long.
     */
    public static final int UWIDTH = 8;
}
